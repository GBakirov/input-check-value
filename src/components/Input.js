import { useState } from "react";
const Input = (event) => {
  const [message, setMessage] = useState("");

  const checkValue = (event) => {
    const value = parseInt(event.target.value);
    if (value < 0) {
      event.nativeEvent.path[1].elements[1].disabled = true;
      event.nativeEvent.path[1].elements[1].attributes[2].value =
        "cursor: not-allowed";
    } else {
      event.nativeEvent.path[1].elements[1].disabled = false;
      event.nativeEvent.path[1].elements[1].attributes[2].value =
        "cursor: pointer";
    }
  };

  const check = (props) => {
    if (isNaN(props)) {
      setMessage("Hech qanday son yoq ");
    } else if (props % 2 === 0 && props !== 0) {
      setMessage(`${props} juft son.`);
    } else if (props % 2 === 1) {
      setMessage(`${props} toq son.`);
    } else {
      setMessage(`${props}  toq ham emas juft ham emas`);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const value = parseInt(event.target.elements[0].value);
    check(value);
  };
  return (
    <div>
      <form
        onSubmit={handleSubmit}
        onChange={checkValue}
        className="form-group"
      >
        <input className="form-control" type="number" id="number" />
        <h4>{message}</h4>
        <button
          type="submit"
          className="btn btn-primary"
          style={{
            cursor: "pointer",
          }}
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default Input;
